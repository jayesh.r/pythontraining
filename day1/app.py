import os
from flask import Flask, jsonify, render_template, request
from sqlalchemy import create_engine, MetaData, Table, Column, Integer, String

app = Flask(__name__)
engine = create_engine('sqlite:///college.db', echo=True)


meta = MetaData()
user = Table('user', meta, Column('id', Integer, primary_key=True),
             Column('name', String), Column('salary', Integer))

meta.bind = engine
meta.create_all()

@app.route('/')
def home():
    return 'this is homepage'

@app.route('/create', methods = ['POST'])
def create():
    name = request.form['name']
    salary = request.form['salary']

    connect = engine.connect()
    insert = user.insert().values(name = name, salary = salary)
    print('inserted : ', insert)
    result = connect.execute(insert)
    print('result: ', result)
    return 'Create successfull '


    

@app.route('/read')
def read():
    s = user.select()
    connect = engine.connect()
    data = connect.execute(s)
    for row in data:
        print(row)
    print(type(data))
    return 'read'

@app.route('/update',methods= ['PUT'])
def update():
    connect = engine.connect()
    pre, nex = request.form['pre'], request.form['nex']
    print(pre, nex)
    query = user.update().where(user.c.name == pre).values(name= nex)
    connect.execute(query)
    s = user.select()
    connect.execute(s).fetchall()
    return 'update success'

@app.route('/delete', methods = ['DELETE'])
def delete():
    connect = engine.connect()
    var = request.form['toDel']
    query = user.delete().where(user.c.name == var)
    connect.execute(query)
    s = user.select()
    connect.execute(s).fetchall()

    return 'delete success'


if __name__ == '__main__':
    app.run(debug = True)
    